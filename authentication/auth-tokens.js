const jwt = require("jsonwebtoken");
const _ = require('lodash')
const privateKey = require('../constants/secretVars')// bettter can be given enviromeent name like prod, uat,debug etc so that it can pick from host


exports.generateAccessToken =  function (user) {
    let accesstoken =  jwt.sign(user, privateKey.accesTokenSecrets, {expiresIn: "15m"});
    return accesstoken
}

//not adding session concept in the code for validating use cases like single sesson login
//not storing tokens in db, storing in in-memory
let refreshTokens = []
exports.generateRefreshToken = function (user) {
    const refreshToken =
        jwt.sign(user, privateKey.refreshTokenSecrets, {expiresIn: "20m"})
    refreshTokens.push(refreshToken)
    return refreshToken
}
exports.validateToken = function (token){
    return _.includes(refreshTokens, token)
}

exports.removeExsitingToken = function (token){
    refreshTokens = refreshTokens.filter( (c) => c !== token)
}
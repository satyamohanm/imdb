const bcrypt = require('bcrypt');
const saltRounds = 10;

module.exports = {
    getHashedpassword,
    validatePassword
}

function getHashedpassword(password) {
    return new Promise((resolve, reject) => {
        bcrypt.hash(password, saltRounds, (err, hash) => {
            return resolve(hash);
        })
    })
}

async function validatePassword(reqPassword, userPassword){
    try{
        const hash =  await bcrypt.hashSync(reqPassword, saltRounds);
        const comparePassword = await bcrypt.compare(reqPassword, userPassword);
        return comparePassword
    }catch (e) {
        return e
    }
}

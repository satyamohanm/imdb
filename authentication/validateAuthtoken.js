const privateKey = require('../constants/secretVars')
const _ = require('lodash')
const jwt = require("jsonwebtoken");

let validateToken = function (req, res, next) {
    const token = _.get(req, ['rawHeaders','1'])

    if (token == null) res.send("Invalid Authentication");
    jwt.verify(token, privateKey.accesTokenSecrets, (err, user) => {
        if (err) {
            res.status(403).send("Token invalid Or no validation token")
        } else {
            req.user = user
            next();
        }
    })
}

module.exports = {
    validateToken
}
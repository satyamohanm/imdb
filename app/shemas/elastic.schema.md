curl -XPUT "http://localhost:9200/movies" -H 'Content-Type: application/json' -d'
{
  "mappings": {
   
      "dynamic_templates": [
         { "fields": {
            "match": "doc*",
            "mapping": {
                  "type": "keyword"
                }
          }
        }
      ]
    
  }
}'
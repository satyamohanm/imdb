let sortQuery = {
    index: "movies",

    "body": {
        "_source": ["doc.title", "doc.rated"],
        "query": {
            "match_all": {}
        },
        "sort": [{
            "doc.rated": {
                "order": "desc"
            }
        }, {
            "doc.update.vote.upvote": {
                "order": "desc"
            }
        }, {
            "doc.released_date": {
                "order": "desc"
            },
        }],
        size: 500
    }
}


module.exports = {
    sortQuery
}
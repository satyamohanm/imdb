const express = require('express');
const app = express();
const bcrypt = require('bcrypt');
const _ = require('lodash')
const userUtils = require('./authentication/user-utils.js');
const tokenUtils = require('./authentication/auth-tokens')
const movieRouters = require('./app/routes/movie-router')
let users=[];
const cfg = require('./config/config');
const mongoose = require('mongoose');
const { MongoClient } = require('mongodb');
const elasticsearch = require('elasticsearch');
const client = new elasticsearch.Client({
    host: 'localhost:9200',
    log: 'trace',
    apiVersion: '7.2', // use the same version of your Elasticsearch instance
});

client.ping({
    // ping usually has a 3000ms timeout
    requestTimeout: 1000
}, function (error) {
    if (error) {
        console.trace('elasticsearch cluster is down!');
    } else {
        console.log('All is well');
    }
});
console.log("----------")
app.use(express.json())
app.use('/movies',movieRouters);

app.get('/users', (req, res) => {
    return res.json("users");
})
app.post('/register', async (req, res) => {
    try {
        console.log(__dirname)
        let hashedPassword = await userUtils.getHashedpassword(req.body.password)
        let user = {name: req.body.name, password: hashedPassword}
        users.push(user);
        res.status(201).send()
    } catch (e) {
        console.log(e)
        res.status(500).status()
    }
})

app.post('/register/login', async (req, res) => {
    const user = _.find(users, user => user.name === req.body.name)
    if (_.isEmpty(user))
        return res.status(400).send('cannot find user')

    try {
        if (await userUtils.validatePassword(req.body.password, user.password)) {
            const accessToken =  tokenUtils.generateAccessToken({user: req.body.name})
            const refreshToken = tokenUtils.generateRefreshToken({user: req.body.name})
            return res.json({accessToken: accessToken, refreshToken: refreshToken})
        }
        res.send("given pass word is wrong")
    } catch (e) {
        console.log("validation failes", e.message)
        res.status(500).send();
    }
})


app.post("/refreshToken", (req,res) => {
    if (!tokenUtils.validateToken(req.body.token)) res.status(400).send("Refresh Token Invalid")
   tokenUtils.removeExsitingToken(req.body.token)
    //remove the old refreshToken from the refreshTokens list
    const accessToken = tokenUtils.generateAccessToken ({user: req.body.name})
    const refreshToken = tokenUtils.generateRefreshToken ({user: req.body.name})
    //generate new accessToken and refreshTokens
    res.json ({accessToken: accessToken, refreshToken: refreshToken})
})




app.listen(4000)